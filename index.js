// JS COMMENTS Ctrl+/
// They are important, acts as a guide for the programmer
// Comments are disregarded no matter how long it may be
	// Multi-Line Comments ctrl+shift+/
	/*This
	is
	a 
	multi
	line
	comment*/

// STATEMENTS
// Programming instruction that we tell the computer to perform.
// Statements usually end with semi colon (;)

// SYNTAX 
// It is the set of rules that describes how statements must be constructed




// alert("Hello Again!");

// This is a statement with a correct syntax
console.log("Hello World!");

// JS is a loose type programming language
console. log ( " Hello World!"  );

// And also with this
console.
log
(
"Hello Again"
)

// [Section] Variables
// It is used as a container or storage

// Declaring variables - tells our device that a variable name is created and is ready store data.
// Syntax - let/const variableName;

// "let" is a keyword that is usually used to declare a variable.

let myVariable;
let hello;

console.log(myVariable);
console.log(hello);

// = stands for for initailization, that means giving a value to a variable

// Guidlines for declaring a variable
/*
1. use "let" keyword followed by a variable name and then followed again by the assignment =.
2. Variable name should start with a lowercase letter.
3. for constant variable we are using "const" keyword. 
4. variable name should be comprehensive or descriptive.
5. Do not use spaces on the declared variables
*/

// DIFFERENT CASING STYLES
// cammel case - thisIsCammelCasing >most famous<
// snake case - this_is_snake_casing
// kebab case - this-is-kebab-casing

// this is a good variable name
let firstName = "Michael"; 

// this is a bad variable name
let pokemon = 25000;


// Declaring and initalizing variables
// Syntax -> let/const variableName = value;

let productName = "desktop computer";
console.log(productName);

let	productPrice = 18999;
console.log(productPrice);

// re-assigning a value to a variable
productPrice = 25000;
console.log(productPrice);
console.log(productPrice);
console.log(productPrice);


let friend = "kate";
friend = "jane";
console.log(friend)

let supplier;
supplier = "John Smith Tradings"
console.log(supplier)

supplier = "Zuitt Store"
console.log(supplier)

// let vs const
// let variable values can be change, we can re-assign new values.
// const variable values cannot be changed.

const hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

// local/global scope variable

let outerVariable = "hi"; //this is a global variable

{
	let innerVariable = "hello again";//this is a global variable
	console.log(innerVariable);
	console.log(outerVariable);
}

console.log(outerVariable);
// console.log(innerVariable); this will throw error, inner variable is not accesible since it is enclose with curly braces.

// MULTIPLE VARIABLE DECLARATION
// Multiple variables may be declared in one line.
// Convinient and it is easier to read the code.

/*let productCode = "DC017";
let productBrand = "Dell";*/

let productCode = "DC01" , productBrand = "Dell";
console.log(productCode, productBrand);

// let is a reserved keyword, we will be having an error
/*const let = "hello";
console.log(let);*/

// [SECTION] Data types

// Strings - are series of characters that creates a word.

let country = "Philippines";
let province = "Metro Manila";

// Concatenating String. using + symbol
// Combining string values

// Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// escape character (\)
//  "\n" refers to creating a new line between text

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// numbers
// Integers or whole numbers

let headCount = 26;
console.log(headCount);

// Decimals or Fractions
let grade =98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter " + grade);

// Bollean
// we have 2 boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct)

// Arrays
// Are special kind of data type 
// used to store multiple values with the same data types
// Synatax -> let/const arrayName = [elementA, elementB]

let grades = [98.7, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects 
// Holds properties that describes the variable
// Syntax -> let/const objectName = {propertyA: value, propertB: Value}

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["0123456789", "987564321"],
	address: {
		houseNumber: "345", 
		city: "Manila"
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7, 
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}

console.log(myGrades);

// Checking of data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

// in programming we always start counting from 0
const anime = ["one piece", "one punch man", "attack on titan"];
// anime = ["kimetsu no yaiba"];
anime [0] = "kimetsu no yaiba";

console.log(anime);